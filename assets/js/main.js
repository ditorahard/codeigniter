var Site = {

	init: function(){
		$('html').removeClass('no-js');

		if($('#map').length)
			Site.map();

		if($('#zoom').length)
			Site.zoom();


	},

	zoom: function(){
		$('#zoom').elevateZoom({
			zoomWindowWidth: 300,
			zoomWindowHeight: 300,
			easing: true
		});
	},

	map: function(){
		
		var peta = new GMaps({
			  div: '#map',
			  lat: -12.043333,
			  lng: -77.028333
			});
 

		peta.addMarker({
		  lat: -12.043333,
		  lng: -77.028333,
		  title: 'Lima',
		  click: function(e) {
		    alert('You clicked in this marker');
		  }});
	}

};

$(function (){
	Site.init();

});
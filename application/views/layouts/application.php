<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/').'/normalize.min.css';?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/').'/bootstrap.css';?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/').'/style.css';?>">

	<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/HTML5Shiv.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="user-nav">
	<!--<button class="btn">home</button>-->

	
		<div class="container">
			<ul>
				<li>Welcome guest!</li>
				<li><i><a href="#myModal" data-toggle="modal">Log in</a>/
					<a href="#myModal2" data-toggle="modal">Sign Up</a></i></i>
				</li>
				<li><?php echo anchor('/hello/cart', 'Cart');?><i class="icon-shopping-cart icon-white"></i></li>
				<li><?php echo anchor('/hello/about', 'About');?></li>
			</ul>
		</div>	
	</nav>
	<header>
		<div class="container">
			<div class ="row">
				<div class="span4 logo">
					<div class="main-nav"><img src="<?php echo base_url('assets/img').'/logo.png';?>">
					</div>
				</div>
				<div class="span8">
				<div class="main-nav">
				<ul>
						<li><?php echo anchor('/hello/index', 'Home');?></li>
						<li><a href="">Baju</a></li>
						<li><a href="">Celana</a></li>
						<li><a href="">Sepatu</a></li>
						<li><a href="">Aksesoris</a></li>
					</ul>
				</div>
				</div>
			</div>
		</div>
	</header>

	<?php $this->load->view($view); ?>
							
	

<footer>
	<div class="container">
	<div class="row">
	<a href="" class="footercon"><i class="icon-off icon-white"></i><p>Sign Out</p></a>
	<a href="" class="footercon"><i class="icon-search icon-white"></i><p>Search</p></a>
	</div>
	</div>
	<ul>
		<li><?php echo anchor('/hello/about', 'About');?></li>
		<li><a href="">Privacy Policy</a></li>
		<li><?php echo anchor('/hello/contact', 'Contact');?></li>
	</ul>

</footer>

						<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h2 class="form-signin-heading black">Please sign in</h2>
							  </div>
							  <div class="modal-body">
							 	 <form class="form-signin">
							 	  <input type="text" class="input-block-level" placeholder="Email address">
							        <input type="password" class="input-block-level" placeholder="Password">
							        <label class="checkbox">
							        	
							          <input class="black" type="checkbox" value="remember-me">Remember me

							        </label>
							        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
							      </form>
							  </div>
							  <!--<div class="modal-footer">
							    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							    <button class="btn btn-primary">Save changes</button>
							  </div>-->
							</div>

							<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h2 class="form-signin-heading black">Sign up just in a view steps</h2>
							  </div>
							  <div class="modal-body">
							 	 <form class="form-signin">
							 	 	<input type="text" class="input-block-level" placeholder="Your Name">
							 	  	<input type="text" class="input-block-level" placeholder="Email address">
							        <input type="password" class="input-block-level" placeholder="Password">
							        <label class="checkbox">
							       
							        <input class="black" type="checkbox" value="remember-me">Remember me every time I go to this site
							        
							        </label>
							        <button class="btn btn-large btn-primary" type="submit">Sign up</button>
							      </form>
							  </div>
							  <!--<div class="modal-footer">
							    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							    <button class="btn btn-primary">Save changes</button>
							  </div>-->
							</div>



		<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js').'/jquery-1.10.1.min.js';?>"><\/script>')</script>
	<script src="<?php echo base_url('assets/js').'/bootstrap.min.js';?>"></script>
	<script src="<?php echo base_url('assets/js').'/jquery.elevateZoom-2.5.5.min.js';?>"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url('assets/js').'/gmaps.js';?>"></script>
	<script src="<?php echo base_url('assets/js').'/main.js';?>"></script>
<!--	<p>Template Starter</p>		-->
	</body>
	</html>

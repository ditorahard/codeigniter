<div class="container">
		<div class="cart">
			<h2>Your Cart</h2>
			<div class="row">
				<div class="span6 property">
					Product Name
					<div class="cart-properties">
					 	Poshboy
					 	<br>
					 	<br>
					 	<br>
					</div>
				</div>
				<div class="span2 property">
					Price
					<div class="cart-properties">
						200.000
						<br>
						<br>
						<br>
					</div>
				</div>
				<div class="span2 property">
					Quantity
					<div class="cart-properties">
						1
						<br>
						<br>
						<br>
					</div>
				</div>
				<div class="span2 property">
					Subtotal
					<div class="cart-properties">
						Subtotal 200.000
						Shipping 0
						Total 200.000
					</div>
				</div>
			</div>
		</div>
	</div>
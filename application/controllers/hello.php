<?php
class Hello extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$data['view'] = 'hello/index';
		$this->load->vars($data);
		$this->load->view('layouts/application');
	}
	public function world(){
		echo "world";
	}

	public function about()
	{
		# code...
		$data['view'] = 'hello/about';
		$this->load->vars($data);
		$this->load->view('layouts/application');

	}

	public function cart()
	{
		# code...
		$data['view'] = 'hello/cart';
		$this->load->vars($data);
		$this->load->view('layouts/application');

	}

	public function contact()
	{
		# code...
		$data['view'] = 'hello/contact';
		$this->load->vars($data);
		$this->load->view('layouts/application');

	}

	public function detail()
	{
		# code...
		$data['view'] = 'hello/detail';
		$this->load->vars($data);
		$this->load->view('layouts/application');

	}


}
